package com.android.appium.runner;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(
		
		features = "./src/test/java/com/android/appium/features",
		glue = {"com.android.appium.stepdefinitions","com.android.appium.hooks"},
		dryRun = false, 
		plugin = { "pretty", "html:target/Cucumber/CucumberHTMLReport.html", "html:target/Cucumber/CucumberXMLReport.xml" }, 
		monochrome = true, 
		tags = "(@Animation) or (Not@Accessibility)")
public class TestRunner {

}
