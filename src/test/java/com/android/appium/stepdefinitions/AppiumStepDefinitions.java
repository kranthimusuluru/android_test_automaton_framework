package com.android.appium.stepdefinitions;

import com.android.appium.drivermanager.DriverManager;
import com.android.appium.screens.LandingScreen;
import com.android.filereadermanager.FileReaderManager;

import io.appium.java_client.android.AndroidDriver;
import io.cucumber.java.en.Given;

public class AppiumStepDefinitions {

	FileReaderManager frm;
	AndroidDriver driver;
	LandingScreen ls;

	public AppiumStepDefinitions() {
		frm = new FileReaderManager();
		driver = DriverManager.initializeDriver();
		ls = new LandingScreen(driver);

	}

	// ################### Step Definitions Implementaton Starts Here #############################

	@Given("I want to write a step with precondition")
	public void i_want_to_write_a_step_with_precondition() {
		ls.tapOnAccessibility();
		driver.navigate().back();
	}
}
