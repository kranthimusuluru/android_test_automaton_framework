package com.android.filereadermanager;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

public class FileReaderManager {

	static FileInputStream fis;
	static Properties prop;

	public FileReaderManager() {
		
		try {
			File f = new File(System.getProperty("user.dir")+File.separator+"Config"+File.separator+"config.properties");
			fis = new FileInputStream(f);
			prop = new Properties();
			prop.load(fis);
			fis.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	// ###################### APPIUM SERVER CONFIGURATIONS ################
	/**
	 * @return Appium Server port number
	 */
	public static String getAppiumServerPortWin() {
		return prop.getProperty("appiumServerPort_Win");
	}
	
	public static String getAppiumServerIPWin() {
		return prop.getProperty("appiumServerIP_Win");
	}
	
	/**
	 * @return Appium JS Path in Windows machine
	 */
	public static String getAppiumJsPathWin() {
		return prop.getProperty("appiumJsPath_Win");
	}
	
	/**
	 * @return Node .exe file path
	 */
	public static String getNodeExePathWin() {
		return prop.getProperty("nodeExecutabalePath_Win");
	}


	// ########################### WINDOWS OS - ANDROID APP CONFIGURATIONS #########
	
	/**
	 * @return Apk File Name
	 */
	public static String getAPKName_WIN() {
		return prop.getProperty("apkFileName_WIN");
	}
	
	/**
	 * @return
	 */
	public static String getPlatformName_WIN() {
		return prop.getProperty("platformName_WIN");
	}
	
	/**
	 * @return
	 */
	public static String getAutomationName_WIN() {
		return prop.getProperty("automationName_WIN");
	}
	
	/**
	 * @return
	 */
	public static String getDeviceName_WIN() {
		return prop.getProperty("deviceName_WIN");
	}
	
	/**
	 * @return
	 */
	public static String getUdid_WIN() {
		return prop.getProperty("udid_WIN");
	}
	
	/**
	 * @return
	 */
	public static String getAvd_WIN() {
		return prop.getProperty("avd_WIN");
	}
	
	/**
	 * @return
	 */
	public static String getAvdLaunchTimeout_WIN() {
		return prop.getProperty("avdLaunchTimeout_WIN");
	}
	
	/**
	 * @return
	 */
	public static String getNewCommandTimeout_WIN() {
		return prop.getProperty("newCommandTimeout_WIN");
	}
	
	// ############################ MAC OS Android App Configurations ###########
	
	/**
	 * @return Apk File Name
	 */
	public static String getAPKName_MAC() {
		return prop.getProperty("apkFileName_MAC");
	}
	
	/**
	 * @return
	 */
	public static String getPlatformName_MAC() {
		return prop.getProperty("platformName_MAC");
	}
	
	/**
	 * @return
	 */
	public static String getautomationName_MAC() {
		return prop.getProperty("automationName_MAC");
	}
	
	/**
	 * @return
	 */
	public static String getDeviceName_MAC() {
		return prop.getProperty("deviceName_MAC");
	}
	
	/**
	 * @return
	 */
	public static String getUdid_MAC() {
		return prop.getProperty("udid_MAC");
	}
	
	/**
	 * @return
	 */
	public static String getAvd_MAC() {
		return prop.getProperty("avd_MAC");
	}
	
	/**
	 * @return
	 */
	public static String getAvdLaunchTimeout_MAC() {
		return prop.getProperty("avdLaunchTimeout_MAC");
	}
	
	/**
	 * @return
	 */
	public static String getNewCommandTimeout_MAC() {
		return prop.getProperty("newCommandTimeout_MAC");
	}
	
	
	// ############################# IOS Related Methods ######################
	
	/**
	 * @return IOS app name
	 */
	public static String getAppFileName() {
		return prop.getProperty("appFileName");
	}

	
	
	
}