package com.android.appium.drivermanager;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.remote.DesiredCapabilities;

import com.android.appium.servermanager.AppiumServerManager;
import com.android.filereadermanager.FileReaderManager;
import io.appium.java_client.android.AndroidDriver;

public class DriverManager {

	static URL url;
	static DesiredCapabilities dcap;
	static FileReaderManager frm;

	/**
	 * @return
	 * @return Appium Driver object
	 */
	public static AndroidDriver initializeDriver() {
		dcap = new DesiredCapabilities();
		String os = System.getProperty("os.name");
		frm = new FileReaderManager();

		if (os.contains("Windows")) {
			AppiumServerManager.startServer_WIN();
			dcap.setCapability("platformName", FileReaderManager.getPlatformName_WIN());
			dcap.setCapability("appium:automationName", FileReaderManager.getAutomationName_WIN());
			dcap.setCapability("appium:deviceName", FileReaderManager.getDeviceName_WIN());
			dcap.setCapability("appium:udid", FileReaderManager.getUdid_WIN());
			dcap.setCapability("avd", FileReaderManager.getAvd_WIN());
			dcap.setCapability("avdLaunchTimeout", Integer.parseInt(FileReaderManager.getAvdLaunchTimeout_WIN()));
			dcap.setCapability("appium:app", System.getProperty("user.dir") + File.separator + "APK" + File.separator+ FileReaderManager.getAPKName_WIN());
			dcap.setCapability("newCommandTimeout", Integer.parseInt(FileReaderManager.getNewCommandTimeout_WIN()));
			try {
				url = new URL("http://0.0.0.0:4723/");
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
		} else if (os.contains("Mac")) {
			AppiumServerManager.startServer_MAC();
			dcap.setCapability("platformName", FileReaderManager.getPlatformName_MAC());
			dcap.setCapability("appium:automationName", FileReaderManager.getautomationName_MAC());
			dcap.setCapability("appium:deviceName", FileReaderManager.getDeviceName_MAC());
			dcap.setCapability("appium:udid", FileReaderManager.getUdid_MAC());
			dcap.setCapability("avd", FileReaderManager.getAvd_MAC());
			dcap.setCapability("avdLaunchTimeout", Integer.parseInt(FileReaderManager.getAvdLaunchTimeout_MAC()));
			dcap.setCapability("appium:app", System.getProperty("user.dir") + File.separator + "APK" + File.separator+ FileReaderManager.getAPKName_MAC());
			dcap.setCapability("newCommandTimeout", Integer.parseInt(FileReaderManager.getNewCommandTimeout_MAC()));
			try {
				url = new URL("http://127.0.0.1:4723/");
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
		}
		return new AndroidDriver(url, dcap);
	}

}
