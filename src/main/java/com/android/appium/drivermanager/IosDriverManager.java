package com.android.appium.drivermanager;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.remote.DesiredCapabilities;

import com.android.filereadermanager.FileReaderManager;

import io.appium.java_client.AppiumBy;
import io.appium.java_client.ios.IOSDriver;

public class IosDriverManager {
	
	
	public static void main(String[] args) throws MalformedURLException {
		FileReaderManager frm = new FileReaderManager();
		DesiredCapabilities dcap = new DesiredCapabilities();
		dcap.setCapability("platformName", "ios");
		dcap.setCapability("appium:automationName", "XCUITest");
		dcap.setCapability("appium:deviceName", "iPhone 15 Pro");
		dcap.setCapability("appium:udid", "1C62FDE2-FE86-4137-A20A-3F492CD52876");
		dcap.setCapability("appium:app", System.getProperty("user.dir") + File.separator + "APP" + File.separator+ FileReaderManager.getAppFileName());
		dcap.setCapability("appium:simulatorStartupTimeout",240000 );
		URL url = new URL("http://127.0.0.1:4723/");
		IOSDriver idriver = new IOSDriver(url, dcap);
		idriver.findElement(AppiumBy.id("Buttons")).click();
		idriver.navigate().back();
		
		
		
		
		
	}

}
