package com.android.appium.servermanager;

import java.io.File;

import com.android.filereadermanager.FileReaderManager;

import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import io.appium.java_client.service.local.flags.GeneralServerFlag;

public class AppiumServerManager {

	static AppiumDriverLocalService server;
	
	/**
	 * 
	 */
	public static void startServer_WIN() {

//		AppiumServiceBuilder builder = new AppiumServiceBuilder();
//		builder
//				.withAppiumJS(new File(FileReaderManager.getAppiumJsPathWin()))
//				.usingDriverExecutable(new File(FileReaderManager.getNodeExePathWin()))
//				.usingPort(Integer.parseInt(FileReaderManager.getAppiumServerPortWin()))
//				.withIPAddress(FileReaderManager.getAppiumServerIPWin())
//				.withLogFile(new File("AppiumLog.txt"))
//				.withArgument(GeneralServerFlag.LOCAL_TIMEZONE);
//		server = AppiumDriverLocalService.buildService(builder);
//		server.start();
		System.out.println("################### APPIUM SERVER IS NOT RUNNING PROGRAMATICALLY IN WINDOWS ###############");
	}
	
	/**
	 * 
	 */
	public static void startServer_MAC() {
		System.out.println("############# There is an issue in the MAC OS Appium Server start programatically #############");
		
	}
	
	/**
	 * 
	 */
	public static void stopServer() {
		server.stop();
	}
}
