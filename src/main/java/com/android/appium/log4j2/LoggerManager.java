package com.android.appium.log4j2;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LoggerManager {

	private static final Logger log = LogManager.getLogger(LoggerManager.class);
	
	public static Logger getLogger() {
		return log;
	}
}
