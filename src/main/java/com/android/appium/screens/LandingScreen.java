package com.android.appium.screens;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.PageFactory;

import com.android.appium.helper.Helper;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

/**
 * @author kranthimusuluru
 * @description Landing Screen POM class with WebElements and Action Methods
 *
 */
public class LandingScreen extends Helper{

	AndroidDriver driver;
	
	public LandingScreen(AndroidDriver driver) {
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
//##################### Landing Screen App Elements Starts Here #########################
	
	@AndroidFindBy(accessibility = "Accessibility")
	@CacheLookup private WebElement accessibility;
	
	@AndroidFindBy(uiAutomator = "new UiSelector().text(\"Animation\")")
	@CacheLookup private WebElement animation;
	
	@AndroidFindBy(uiAutomator = "new UiSelector().text(\"App\")")
	@CacheLookup private WebElement app;
	
	@AndroidFindBy(uiAutomator = "new UiSelector().text(\"Content\")")
	@CacheLookup private WebElement content;
	
	@AndroidFindBy(uiAutomator = "new UiSelector().text(\"Graphics\")")
	@CacheLookup private WebElement graphics;
	
	@AndroidFindBy(uiAutomator = "new UiSelector().text(\"Media\")")
	@CacheLookup private WebElement media;
	
	@AndroidFindBy(uiAutomator = "new UiSelector().text(\"NFC\")")
	@CacheLookup private WebElement nFC;
	
	@AndroidFindBy(uiAutomator = "new UiSelector().text(\"OS\")")
	@CacheLookup private WebElement os;
	
	@AndroidFindBy(uiAutomator = "new UiSelector().text(\"Preference\")")
	@CacheLookup private WebElement preference;
	
	@AndroidFindBy(uiAutomator = "new UiSelector().text(\"Text\")")
	@CacheLookup private WebElement text;
	
	@AndroidFindBy(uiAutomator = "new UiSelector().text(\"Views\")")
	@CacheLookup private WebElement views;
	
	
//##################### Landing Screen App Elements Starts Here #########################	

	/**
	 * @author kranthimusuluru
	 * @description clicks the Accessibility element on the Landing screen
	 */
	public void tapOnAccessibility() {
		click_Helper(driver, accessibility, "Accessibility");
	}
	
	/**
	 * @author kranthimusuluru
	 * @description clicks the Animation element on the Landing screen
	 */
	public void tapOnAnimation() {
		click_Helper(driver, animation, "Animation");
	}
	
	/**
	 * @author kranthimusuluru
	 * @description clicks the App element on the Landing Screen
	 */
	public void tapOnApp() {
		click_Helper(driver, app, "App");
	}
	
	/**
	 * 
	 */
	public void tapOnContent() {
		click_Helper(driver, content, "Content");
	}
	
	/**
	 * 
	 */
	public void tapOnGraphics() {
		click_Helper(driver, graphics, "Graphics");
	}
	
	/**
	 * 
	 */
	public void tapOnMedia() {
		click_Helper(driver, media,"Media");
	}
	
	/**
	 * 
	 */
	public void tapOnNFC() {
		click_Helper(driver, nFC, "NFC");
	}
	
	/**
	 * 
	 */
	public void tapOnOS() {
		click_Helper(driver, os, "OS");
	}
	
	/**
	 * 
	 */
	public void tapOnPreference() {
		click_Helper(driver, preference,"Preference");
	}
	
	/**
	 * 
	 */
	public void tapOnText() {
		click_Helper(driver, text, "Text");
	}
	
	/**
	 * 
	 */
	public void tapOnViews() {
		click_Helper(driver, views, "Views");
	}
}
