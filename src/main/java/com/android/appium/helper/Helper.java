package com.android.appium.helper;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.android.appium.log4j2.LoggerManager;
import com.google.common.collect.ImmutableMap;

import io.appium.java_client.android.AndroidDriver;

public class Helper extends ActionsHelper{
	
	public Helper() {
		super();
	}
//	Logger object is created in this class by calling getLogger() method in the class LoggerManager
	static Logger log = LoggerManager.getLogger();

	
//	################################ COMMON WAIT METHODS STARTS HERE ######################################
	
	/**
	 * @return 
	 * 
	 */
	public boolean verifyElePresent(AndroidDriver driver, WebElement ele, String logMsg) {
		try {
			WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(30));
			wait.until(ExpectedConditions.visibilityOf(ele));
			return true;
		}catch (Exception e) {
			log.info(logMsg + "<== Element not Present");
			return false;
		}
		
	}
	
//	################################ COMMON WAIT METHODS STARTS HERE ######################################
//	################################ COMMON ACTION METHODS STARTS HERE ####################################
	
	/**
	 * @param ele need webelement to perform tap action
	 * @param logMsg need to log the action performed
	 */
	public void click_Helper(AndroidDriver driver, WebElement ele, String logMsg) {
		if(verifyElePresent(driver, ele, logMsg)) {
			ele.click();
			log.info("Performed tap action on => " + logMsg);
		}
		
	}
	
//	################################ COMMON ACTION METHODS ENDS HERE ######################################
		
//	################################ SWIPE GESTURE ACTION METHODS STARTS HERE #############################
	
	
	/**
	 * @param driver Takes Driver object
	 * @param ele Takes WebElement object
	 * @param percentage The size of the swipe as a percentage of the swipe area size. Valid values must be float numbers in range 0..1, where 1.0 is 100%. Mandatory value
	 * @param logMsg Enter the log message to be logged
	 */
	public void swipeEleUp_Helper(AndroidDriver driver,WebElement ele,double percentage, String logMsg) {
		Map<String, Object> params = new HashMap<>();
		params.put("elementId", ((RemoteWebElement)ele).getId());
		params.put("direction", "UP");
		params.put("percent", percentage);
		((JavascriptExecutor)driver).executeScript("mobile: swipeGesture", params);
		log.info(logMsg);
	}
	
	/**
	 * @param driver Takes Driver object
	 * @param ele Takes WebElement object
	 * @param percentage The size of the swipe as a percentage of the swipe area size. Valid values must be float numbers in range 0..1, where 1.0 is 100%. Mandatory value
	 * @param logMsg Enter the log message to be logged
	 */
	public void swipeEleDown_Helper(AndroidDriver driver,WebElement ele, double percentage, String logMsg) {
		Map<String, Object> params = new HashMap<>();
		params.put("elementId", ((RemoteWebElement)ele).getId());
		params.put("direction", "DOWN");
		params.put("percent", percentage);
		((JavascriptExecutor)driver).executeScript("mobile: swipeGesture", params);
		log.info(logMsg);
	}
	
	/**
	 * @param driver Takes Driver object
	 * @param ele Takes WebElement object
	 * @param percentage The size of the swipe as a percentage of the swipe area size. Valid values must be float numbers in range 0..1, where 1.0 is 100%. Mandatory value
	 * @param logMsg Enter the log message to be logged
	 */
	public void swipeEleLeft_Helper(AndroidDriver driver,WebElement ele, double percentage, String logMsg) {
		Map<String, Object> params = new HashMap<>();
		params.put("elementId", ((RemoteWebElement)ele).getId());
		params.put("direction", "LEFT");
		params.put("percent", percentage);
		((JavascriptExecutor)driver).executeScript("mobile: swipeGesture", params);
		log.info(logMsg);
	}
	
	/**
	 * @param driver Takes Driver object
	 * @param ele Takes WebElement object
	 * @param percentage The size of the swipe as a percentage of the swipe area size. Valid values must be float numbers in range 0..1, where 1.0 is 100%. Mandatory value
	 * @param logMsg Enter the log message to be logged
	 */
	public void swipeEleRight_Helper(AndroidDriver driver,WebElement ele, double percentage, String logMsg) {
		((JavascriptExecutor)driver).executeScript("mobile: swipeGesture", ImmutableMap.of(
				"elementId", ((RemoteWebElement)ele).getId(), 
				"direction", "RIGHT", 
				"percent", percentage
				));
		log.info(logMsg);
	}
//	################################ SWIPE GESTURE ACTION METHODS ENDS HERE ###############################
	
}
